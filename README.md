Spree APP Store

================
Title

  Example of a rails spree app running on Heroku

## Contributing

If you'd like to contribute, please take a look at the
[instructions](CONTRIBUTING.md) for installing dependencies and crafting a good
pull request.
